package figuri;

public class Circle {
	double radius;
		
	public Circle(double radius){
			this.radius=radius;
		} 
		
		public Circle(){
			this(10);
		}
		
		public double area(){
			return Math.PI*Math.pow(radius, 2);
		}
		
		public double perimeter(){
			return 2*Math.PI*radius;
		}
}
