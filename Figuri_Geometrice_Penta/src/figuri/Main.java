package figuri;
import java.util.Scanner;

public class Main {
	public static void main(String []args){
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose a figure {triangle, circle, rectangle}");
	    String figura = sc.next();
		if(figura.equals("triangle")){
			System.out.println("Give a value for the first side of the triangle");
			try{
				double l1=Double.parseDouble(sc.next());
				System.out.println("Give a value for the second side of the triangle");
				double l2=Double.parseDouble(sc.next());
				System.out.println("Give a value for the third side of the triangle");
				double l3=Double.parseDouble(sc.next());
				Triangle p= new Triangle(l1, l2, l3);
				System.out.println("The area of the triangle is: " + p.area());
				System.out.println("The perimeter of the triangle is: " + p.perimeter());
			}catch(NumberFormatException e){
				System.out.println("Error! Dimensions need to be numbers!");
			}
		}
		else
			if(figura.equals("rectangle")){
				System.out.println("Give a value for the width of the rectangle");
				try{
					double lun=Double.parseDouble(sc.next());
					System.out.println("Give a value for the height of the rectangle");
					double lat=Double.parseDouble(sc.next());
					Rectangle d= new Rectangle(lun, lat);
					System.out.println("The area of the rectangle is: " + d.area());
					System.out.println("The perimeter of the rectangle is: " + d.perimeter());
				}catch(NumberFormatException e){
					System.out.println("Error! Dimensions need to be numbers!");
				}
			}
			else
				if(figura.equals("circle")){
					System.out.println("Give the radius of the circle");
					try{
						double dim=Double.parseDouble(sc.next());
						Circle c= new Circle(dim);
						System.out.println("The area of the circle is: " + c.area());
						System.out.println("The perimeter of the circle is: " + c.perimeter());
					}catch(NumberFormatException e){
						System.out.println("Error! Dimensions need to be numbers!");
					}
				}
				else
					System.out.println("Error! Inexistent figure!");
	}
}
