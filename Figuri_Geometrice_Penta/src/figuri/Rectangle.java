package figuri;

public class Rectangle {
	double width;
	double height;
	
	public Rectangle(double width, double height){
		this.width=width;
		this.height=height;
	} 
	
	public Rectangle(){
		this(10, 10);
	}
	
	public double area(){
		return width*height;
	}
	
	public double perimeter(){
		return 2*width+2*height;
	}
}
