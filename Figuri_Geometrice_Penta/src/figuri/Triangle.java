package figuri;

public class Triangle {
	double l1;
	double l2;
	double l3;
	
	public Triangle(double l1, double l2, double l3){
		this.l1=l1;
		this.l2=l2;
		this.l3=l3;
	} 
	
	public Triangle(){
		this(10, 10, 10);
	}
	
	public double area(){
		double p=(l1 + l2 + l3)/2;
		double a=Math.sqrt(p*(p-l1)*(p-l2)*(p-l3));
		return a;
	}
	
	public double perimeter(){
		return l1 + l2 + l3;
	}
}
